#include <iostream>
#include <map>

bool running = false;

class SEXP {
public:
  enum Type {
    CONS,
    NIL,
    SYMBOL,
    FIXNUM,
    BOOLEAN,
    CHAR,
    STRING,
    PRIMITIVE,
    LAMBDA
  };
  SEXP(Type T) : T(T) {}
  virtual void print() {}
  virtual bool isSelfEvaluating() { return false; }
  bool isa(Type Ty) { return T == Ty; }

protected:
  Type T;
};

class ATOM : public SEXP {
protected:
  ATOM(Type T) : SEXP(T) {}
};

class CONS : public SEXP {
public:
  CONS(SEXP *CAR, SEXP *CDR) : CAR(CAR), CDR(CDR), SEXP(SEXP::CONS) {}
  CONS() : CAR(nullptr), CDR(nullptr), SEXP(SEXP::CONS) {}
  virtual void print() override;

  SEXP *getCAR() { return CAR; }
  void setCAR(SEXP *S) { CAR = S; }
  SEXP *getCDR() { return CDR; }
  void setCDR(SEXP *S) { CDR = S; }

private:
  SEXP *CAR, *CDR;
};

class NIL : public CONS {
public:
  NIL() : CONS() { T = SEXP::NIL; }
  virtual void print() override;
};

NIL NILV;

class SYMBOL : public ATOM {
public:
  SYMBOL(std::string &S) : S(S), ATOM(SEXP::SYMBOL) {}
  virtual void print() override;
  static SYMBOL *getSYMBOL(std::string S);

private:
  std::string S;
  static std::map<std::string, SYMBOL *> SymbolTable;
};

std::map<std::string, SYMBOL *> SYMBOL::SymbolTable;

class FIXNUM : public ATOM {
public:
  FIXNUM(int VAL) : VAL(VAL), ATOM(SEXP::FIXNUM) {}
  virtual void print() override;
  virtual bool isSelfEvaluating() { return true; }
  int getVAL() { return VAL; }

private:
  int VAL;
};

class BOOLEAN : public ATOM {
public:
  virtual void print() override;
  virtual bool isSelfEvaluating() { return true; }
  static BOOLEAN T;
  static BOOLEAN F;

private:
  BOOLEAN(bool VAL) : VAL(VAL), ATOM(SEXP::BOOLEAN) {}
  bool VAL;
};

BOOLEAN BOOLEAN::T(true);
BOOLEAN BOOLEAN::F(false);

class CHAR : public ATOM {
public:
  CHAR(char C) : C(C), ATOM(SEXP::CHAR) {}
  virtual void print() override;
  virtual bool isSelfEvaluating() { return true; }
  char getCHAR() { return C; }

private:
  char C;
};

class STRING : public ATOM {
public:
  STRING(std::string STR) : STR(STR), ATOM(SEXP::STRING) {}
  virtual void print() override;
  virtual bool isSelfEvaluating() { return true; }
  std::string getSTR() { return STR; }

private:
  std::string STR;
};

typedef SEXP *(*PrimitiveProc)(CONS *);
class PRIMITIVE : public SEXP {
public:
  PRIMITIVE(PrimitiveProc FN) : FN(FN), SEXP(SEXP::PRIMITIVE) {}

  virtual void print() override;

  PrimitiveProc FN;
};

class LAMBDA : public SEXP {
public:
  LAMBDA(class CONS *ARGS, class CONS *BODY, class CONS *ENV)
      : ARGS(ARGS), BODY(BODY), ENV(ENV), SEXP(SEXP::LAMBDA) {}
  virtual void print() override;

  class CONS *ARGS, *BODY, *ENV;
};

void CONS::print() {
  std::cout << "(";
  CAR->print();
  std::cout << " . ";
  CDR->print();
  std::cout << ")";
}

SYMBOL *SYMBOL::getSYMBOL(std::string S) {
  if (SymbolTable.find(S) != SymbolTable.end())
    return SymbolTable.find(S)->second;
  SYMBOL *SYM = new SYMBOL(S);
  SymbolTable[S] = SYM;
  return SYM;
}

void NIL::print() { std::cout << "()"; }

void SYMBOL::print() { std::cout << S; }

void FIXNUM::print() { std::cout << VAL; }

void BOOLEAN::print() { std::cout << (VAL ? "#t" : "#f"); }

void CHAR::print() { std::cout << "#\\" << C; }

void STRING::print() { std::cout << "\"" << STR << "\""; }

void PRIMITIVE::print() { std::cout << "<PRIMITIVE " << (void *)this << ">"; }

void LAMBDA::print() { std::cout << "<LAMBDA " << (void *)this << ">"; }

void TERPRI() { std::cout << "\n"; }

BOOLEAN *EQ(SEXP *S1, SEXP *S2) { return S1 == S2 ? &BOOLEAN::T : &BOOLEAN::F; }

SEXP *CAR(CONS *C) { return C->getCAR(); }

SEXP *CDR(CONS *C) { return C->getCDR(); }

class Token {
public:
  enum Type {
    LPAREN,
    RPAREN,
    DOT,
    QUOTE,
    NIL,
    INTEGER,
    SYMBOL,
    CHAR,
    STRING,
    BOOLEAN
  };
  Token(Type T, char C) : T(T), C(C) {}
  Token(Type T, std::string text) : T(T), text(text) {}
  Token(Type T, int N) : T(T), N(N) {}
  Token(Type T) : T(T) {}

  Type getType() { return T; }

  int getInt() { return N; }

  char getChar() { return C; }
  std::string getText() { return text; }

private:
  Type T;
  int N;
  char C;
  std::string text;
};

bool isWhitespace(char C) { return C == ' ' || C == '\n' || C == '\t'; }

bool isDigit(char C) { return C >= '0' && C <= '9'; }

char nextChar() { return std::cin.get(); }

char peek() { return std::cin.peek(); }

void skipWhitespace() {
  while (isWhitespace(peek()))
    nextChar();
  if (peek() == ';') {
    while (peek() != '\n')
      nextChar();
    nextChar();
  }
}

class Lexer {
public:
  Lexer() : hasSaved(false), Saved(Token::NIL) {}
  Token getNextToken();
  void replaceToken(Token T);

private:
  Token Saved;
  bool hasSaved;
};

Token Lexer::getNextToken() {
  if (hasSaved) {
    hasSaved = false;
    return Saved;
  }
  skipWhitespace();
  char Cur = nextChar();
  switch (Cur) {
  case '(':
    if (peek() == ')') {
      nextChar();
      return Token(Token::NIL);
    }
    return Token(Token::LPAREN);
  case ')':
    return Token(Token::RPAREN);
  case '.':
    return Token(Token::DOT);
  case '\'':
    return Token(Token::QUOTE);
  default:
    break;
  }

  if (isDigit(Cur)) {
    int N = 0;
    while (isDigit(Cur)) {
      N *= 10;
      N += Cur - '0';
      Cur = nextChar();
    }
    std::cin.unget();
    return Token(Token::INTEGER, N);
  }

  if (Cur == '"') {
    std::string text = "";
    Cur = nextChar();
    while (Cur != '"') {
      text += Cur;
      Cur = nextChar();
    }
    return Token(Token::STRING, text);
  }

  if (Cur == '#') {
    Cur = nextChar();
    if (Cur == 't' || Cur == 'f') {
      return Token(Token::BOOLEAN, Cur);
    }
    Cur = nextChar();
    return Token(Token::CHAR, Cur);
  }

  std::string text = "";
  while (!isWhitespace(Cur) && Cur != ')') {
    text += Cur;
    Cur = nextChar();
  }
  std::cin.unget();
  return Token(Token::SYMBOL, text);
}

void Lexer::replaceToken(Token T) {
  Saved = T;
  hasSaved = true;
}

Lexer Lex;

SEXP *READ();

CONS *READ_CONS() {
  Token isEmptyList = Lex.getNextToken();
  if (isEmptyList.getType() == Token::RPAREN) {
    return &NILV;
  }
  Lex.replaceToken(isEmptyList);
  SEXP *CAR = READ();
  Token dot = Lex.getNextToken();
  if (dot.getType() == Token::DOT) {
    SEXP *CDR = READ();
    Token rparen = Lex.getNextToken();
    return new CONS(CAR, CDR);
  }
  Lex.replaceToken(dot);
  SEXP *CDR = READ_CONS();
  return new CONS(CAR, CDR);
}

FIXNUM *READ_INTEGER(Token &T) { return new FIXNUM(T.getInt()); }

CHAR *READ_CHAR(Token &T) { return new CHAR(T.getChar()); }

STRING *READ_STRING(Token &T) { return new STRING(T.getText()); }

SYMBOL *READ_SYMBOL(Token &T) { return SYMBOL::getSYMBOL(T.getText()); }

CONS *READ_QUOTE() {
  SEXP *S = READ();
  return new CONS(SYMBOL::getSYMBOL("QUOTE"), new CONS(S, &NILV));
}

NIL *READ_NIL() { return &NILV; }

BOOLEAN *READ_BOOLEAN(Token &T) {
  if (T.getChar() == 't')
    return &BOOLEAN::T;
  return &BOOLEAN::F;
}

SEXP *READ() {
  Token T = Lex.getNextToken();
  switch (T.getType()) {
  case Token::LPAREN:
    return READ_CONS();
  case Token::NIL:
    return READ_NIL();
  case Token::QUOTE:
    return READ_QUOTE();
  case Token::INTEGER:
    return READ_INTEGER(T);
  case Token::CHAR:
    return READ_CHAR(T);
  case Token::STRING:
    return READ_STRING(T);
  case Token::SYMBOL:
    return READ_SYMBOL(T);
  case Token::BOOLEAN:
    return READ_BOOLEAN(T);
  default:
    break;
  }
  return SYMBOL::getSYMBOL("nil");
}

CONS *NEW_ENV() {
  CONS *ENV = new CONS(&NILV, &NILV);
  return ENV;
}

SEXP *N_REF(CONS *EXPR, FIXNUM *N) {
  int n = N->getVAL();
  while (n > 0) {
    EXPR = static_cast<CONS *>(CDR(EXPR));
    n--;
  }
  return CAR(EXPR);
}

SEXP *EVAL(SEXP *EXPR, CONS *ENV);

void DEFINE(SYMBOL *SYM, SEXP *VAL, CONS *ENV) {
  ENV->setCAR(new CONS(new CONS(SYM, VAL), ENV->getCAR()));
}

SEXP *BEGIN(CONS *LST, CONS *ENV) {
  SEXP *RES = nullptr;
  while (LST != &NILV) {
    RES = EVAL(CAR(LST), ENV);
    LST = static_cast<CONS *>(CDR(LST));
  }
  return RES;
}

SEXP *IF(SEXP *COND, SEXP *THEN, SEXP *ELSE, CONS *ENV) {
  SEXP *B = EVAL(COND, ENV);
  if (B == &BOOLEAN::T)
    return EVAL(THEN, ENV);
  else
    return EVAL(ELSE, ENV);
}

SEXP *LOOKUP(SYMBOL *SYM, CONS *ENV) {
  CONS *FRAME = static_cast<CONS *>(ENV->getCAR());
  while (FRAME != &NILV) {
    CONS *VAL_PAIR = static_cast<CONS *>(FRAME->getCAR());
    if (&BOOLEAN::T == EQ(VAL_PAIR->getCAR(), SYM))
      return VAL_PAIR->getCDR();

    FRAME = static_cast<CONS *>(FRAME->getCDR());
  }
  return &NILV;
}

bool isDefine(CONS *EXPR) {
  return EXPR->getCAR() == SYMBOL::getSYMBOL("define");
}

bool isBegin(CONS *EXPR) {
  return EXPR->getCAR() == SYMBOL::getSYMBOL("begin");
}

bool isIf(CONS *EXPR) { return EXPR->getCAR() == SYMBOL::getSYMBOL("if"); }

bool isLambda(CONS *EXPR) {
  return EXPR->getCAR() == SYMBOL::getSYMBOL("lambda");
}

SEXP *PRIM_ADD(CONS *ARGS) {
  int Result = 0;
  while (ARGS != &NILV) {
    FIXNUM *F = static_cast<FIXNUM *>(CAR(ARGS));
    Result += F->getVAL();
    ARGS = static_cast<CONS *>(CDR(ARGS));
  }
  return new FIXNUM(Result);
}

SEXP *PRIM_CONS(CONS *ARGS) {
  return new CONS(CAR(ARGS), CAR(static_cast<CONS *>(CDR(ARGS))));
}

SEXP *PRIM_CAR(CONS *ARGS) { return CAR(static_cast<CONS *>(CAR(ARGS))); }

SEXP *PRIM_CDR(CONS *ARGS) { return CDR(static_cast<CONS *>(CAR(ARGS))); }

SEXP *PRIM_LIST(CONS *ARGS) { return ARGS; }

SEXP *PRIM_EXIT(CONS *ARGS) {
  running = false;
  return &NILV;
}

SEXP *PRIM_DISPLAY(CONS *ARGS) {
  if (CAR(ARGS)->isa(SEXP::STRING)) {
    std::cout << static_cast<STRING *>(CAR(ARGS))->getSTR();
  } else if (CAR(ARGS)->isa(SEXP::CHAR)) {
    std::cout << static_cast<CHAR *>(CAR(ARGS))->getCHAR();
  } else if (CAR(ARGS)->isa(SEXP::FIXNUM)) {
    std::cout << static_cast<FIXNUM *>(CAR(ARGS))->getVAL();
  }
  return &NILV;
}

CONS *EVAL_LIST(CONS *LST, CONS *ENV) {
  if (LST == &NILV)
    return &NILV;
  return new CONS(EVAL(CAR(LST), ENV),
                  EVAL_LIST(static_cast<CONS *>(CDR(LST)), ENV));
}

SEXP *APPLY(SEXP *PROC, CONS *ARGS) {
  if (PROC->isa(SEXP::PRIMITIVE)) {
    return static_cast<PRIMITIVE *>(PROC)->FN(ARGS);
  }
  if (PROC->isa(SEXP::LAMBDA)) {
    LAMBDA *L = static_cast<LAMBDA *>(PROC);
    CONS *ENV = L->ENV;
    CONS *FORMAL_ARGS = L->ARGS;
    CONS *ACTUAL_ARGS = ARGS;
    while (FORMAL_ARGS != &NILV && ACTUAL_ARGS != &NILV) {
      DEFINE(static_cast<SYMBOL *>(CAR(FORMAL_ARGS)), CAR(ACTUAL_ARGS), ENV);
      FORMAL_ARGS = static_cast<CONS *>(CDR(FORMAL_ARGS));
      ACTUAL_ARGS = static_cast<CONS *>(CDR(ACTUAL_ARGS));
    }
    return EVAL(L->BODY, ENV);
  }
  return &NILV;
}

SEXP *EVAL(SEXP *EXPR, CONS *ENV) {
  if (EXPR->isSelfEvaluating())
    return EXPR;
  if (EXPR->isa(SEXP::SYMBOL)) {
    return LOOKUP(static_cast<SYMBOL *>(EXPR), ENV);
  }
  if (EXPR->isa(SEXP::CONS)) {
    CONS *CONS_EXPR = static_cast<CONS *>(EXPR);
    if (isDefine(CONS_EXPR)) {
      CONS *ARGS = static_cast<CONS *>(CDR(CONS_EXPR));
      DEFINE(static_cast<SYMBOL *>(CAR(ARGS)),
             EVAL(CAR(static_cast<CONS *>(CDR(ARGS))), ENV), ENV);
    } else if (isBegin(CONS_EXPR)) {
      return BEGIN(static_cast<CONS *>(CDR(CONS_EXPR)), ENV);
    } else if (isIf(CONS_EXPR)) {
      SEXP *COND = CAR(static_cast<CONS *>(CDR(CONS_EXPR)));
      SEXP *THEN = N_REF(CONS_EXPR, new FIXNUM(2));
      SEXP *ELSE = N_REF(CONS_EXPR, new FIXNUM(3));
      return IF(COND, THEN, ELSE, ENV);
    } else if (isLambda(CONS_EXPR)) {
      CONS *ARGS =
          static_cast<CONS *>(CAR(static_cast<CONS *>(CDR(CONS_EXPR))));
      CONS *BODY = static_cast<CONS *>(
          CAR(static_cast<CONS *>(CDR(static_cast<CONS *>(CDR(CONS_EXPR))))));
      return new LAMBDA(ARGS, BODY, ENV);
    } else {
      return APPLY(EVAL(CAR(CONS_EXPR), ENV),
                   EVAL_LIST(static_cast<CONS *>(CDR(CONS_EXPR)), ENV));
    }
  }
  return &NILV;
}

CONS *GLOBAL_ENV;

void initPrimitives() {
  DEFINE(SYMBOL::getSYMBOL("cons"), new PRIMITIVE(&PRIM_CONS), GLOBAL_ENV);
  DEFINE(SYMBOL::getSYMBOL("car"), new PRIMITIVE(&PRIM_CAR), GLOBAL_ENV);
  DEFINE(SYMBOL::getSYMBOL("cdr"), new PRIMITIVE(&PRIM_CDR), GLOBAL_ENV);
  DEFINE(SYMBOL::getSYMBOL("list"), new PRIMITIVE(&PRIM_LIST), GLOBAL_ENV);
  DEFINE(SYMBOL::getSYMBOL("+"), new PRIMITIVE(&PRIM_ADD), GLOBAL_ENV);
  DEFINE(SYMBOL::getSYMBOL("exit"), new PRIMITIVE(&PRIM_EXIT), GLOBAL_ENV);
  DEFINE(SYMBOL::getSYMBOL("display"), new PRIMITIVE(&PRIM_DISPLAY),
         GLOBAL_ENV);
}

int main() {
  TERPRI();
  std::cout << "Welcome to Symbolics Carnegie Scheme!";
  TERPRI();
  unsigned N = 0;
  GLOBAL_ENV = NEW_ENV();
  initPrimitives();
  running = true;
  while (running) {
    std::cout << N++ << "> ";
    EVAL(READ(), GLOBAL_ENV)->print();
    TERPRI();
  }
  return 0;
}
